console.log(`Hello Mundo 👋🌏`)
// ES6 Updates
// - ES6 is one of the latest versions of writing JavaScript and in fact is one of the major updates
// let and const
// - are ES6 updates too
// - these are new standards of creating variables

// In JavaScript, hoisting allows you to use functions and variables before they are declared
// BUT! This might cause confusion, because of the confusion that var hoisting can create, it is BEST to avoid using varables before they are declared

console.log(varSample)
var varSample = `Hoist me up!`

// if you have used "name" in other parts of the code, you might be surprissed at the output we might get

var name = 'Camille'

if (true) var name = `Hi!`

var name = `Cams`

console.log(`Hi! My Name is: ${name}`)

// Exponent Operator
const firstNum = 8 ** 2
console.log(firstNum)

const secondNum = Math.pow(8, 2)
console.log(secondNum)

const eightPowerOf3 = 8 ** 3
console.log(eightPowerOf3)

let string1 = 'fun'
let string2 = 'Bootcamp'
let string3 = 'Coding'
let string4 = 'JavaScript'
let string5 = 'Zuitt'
let string6 = 'learning'
let string7 = 'love'
let string8 = 'I'
let string9 = 'is'
let string10 = 'in'

/*
Mini Activity #1
  1. create new variables called concatSentence1 and concatSentence2
  2. concatenate and save a resulting string into sentence1
  3. concatenate and save a resulting string into sentence2
      - log both variables in the console and take a screenshot
      - log sentence MUST have spaces and punctuation
*/

let concatSentence1, concatSentence2

concatSentence1 = `${string1} ${string2} ${string3}, ${string4}, ${string5}`
concatSentence2 = `${string6} ${string7} ${string8}, ${string9}, ${string10}!!!`
console.log(`${concatSentence1}  ${concatSentence2}`)

// "", '' => String literals
// template literals
//  - allows us to create strings using our `` backticks and easily embed JavaScript expressions in it
/* 
  It allows us to write strings without using concatenation operator (+) greatly helps with CODE READIBIITY
  */

let sentence1 = `${string1} ${string6} ${string5} ${string3} ${string2} ${string1}`
console.log(sentence1)

// ${} is a placeholder that is used to embed JavaScript expressions when creating strings using template literals
name = `John` // Pre-Template Literals

// ("") or ('')

let message = 'Hello' + name + '! Welcome to programming'

// Strings using template literals
// (``) backticks

message = `Hello ${name}! Welcome to programming!`
console.log(message)

// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition. He won it by solving the problem 8 ** 2 with the solution of ${firstNum}`
console.log(anotherMessage)

let dev = {
  name: `Peter`,
  lastname: `Parker`,
  occupation: `web developer`,
  income: 50000,
  expenses: 60000,
}
console.log(`${dev.name} is a ${dev.occupation}.`)
console.log(
  `His income is ${dev.income} and expenses at ${
    dev.expenses
  }. His current balance is ${dev.income - dev.expenses}.`
)

const interestRate = 0.1
const principal = 1000

console.log(
  `The interest on your savings account, ${dev.name} ${dev.lastname} is ${
    principal * interestRate
  }`
)

// Object Destructuring
/*
  Allows us to unpack properties of objects into distinct variables

  Syntax:
  let/const {propertyNameA, propertyNameB propertyNameC} = object
  */

const person = {
  givenName: `Jane`,
  maidenName: `Dela`,
  familyName: `Cruz`,
}

// Pre-Object Destructuring
// We can access them using dot or square bracket notation
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

// Object Destructuring
const { givenName, maidenName, familyName } = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(
  `Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`
)

function getFullName({ givenName, maidenName, familyName }) {
  console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)

let pokemon1 = {
  namePokemon: 'Bulbasaur',
  type: 'Grass',
  level: 10,
  moves: ['Razor Leaf', 'Tackle', 'Leech Seed'],
}

let { level, type, namePokemon, moves, personality } = pokemon1

console.log(level)
console.log(type)
console.log(namePokemon)
console.log(moves)
console.log(personality) //undefined

let pokemon2 = {
  namePokemon: 'Charmander',
  type: 'Fire',
  level: 11,
  moves: ['Ember', 'Scratch'],
}

//{propertyName: newVariable}
const { namePokemon: nameVar } = pokemon2
console.log(nameVar)

console.log(`${nameVar} goes char char!`)

// Array Destructuring
// - Allows us to 'unpack' elements in arrays into distinct variables
// - Allows us to name array elements with variabels instead of using index numbers
// - Also helps with code readibility

/*
  let/const [variableName, variableName, variableName] = array
*/

const fullName = ['Juan', 'Dela', 'Cruz']

// Pre-Array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}, it's nice to meet you`
)

// ES6 Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)

console.log(
  `hello ${firstName} ${middleName} ${lastName}! It's nice to meet you again`
)

let cardoDalisayBand = ['Cardo', 'Dalisay', 'Juan', 'Dela', 'Cruz']
let [singer1, , singer3, singer4, singer5] = cardoDalisayBand
console.log(singer3)

// Arrow Function
/*
  An alternative way of writing functions in JavaScript
    Syntax:
    let/const variableName = (parameterA, parameterB, parameterC) => {
      console.log(statement)
    }
*/

// Traditional Function
function displayMessage() {
  console.log(`Hello World!`)
}

displayMessage()

// Arrow Function
const hello = () => {
  console.log(`Hello from Arrow Function!`)
}
hello()

// Arrow Function with Parameters
const greet = (friend) => {
  console.log(`Hi ${friend}`)
}

greet(singer5)

// Arrow vs Traditional Function
// Implicit Return
//  - allows us to return from an arrow function without the use of return keywords

// Traditional addNum() function
/*
  function addNum
*/

function addNum(num1, num2) {
  let result = num1 + num2
  return result
}

let sum = addNum(5, 10)
console.log(sum)

let subNum = (num1, num2) => num1 - num2

let difference = subNum(10, 5)
console.log(difference)

/*
  Mini Activity #3

  create an addition, subtraction, multiplication, and division arrow function. Use 7 and 5 as your arguments. Log the total in your console.
*/

let addition = (num1, num2) => num1 + num2
let subtraction = (num1, num2) => num1 - num2
let multiplication = (num1, num2) => num1 * num2
let division = (num1, num2) => num1 / num2

console.log(addition(7, 5))
console.log(subtraction(7, 5))
console.log(multiplication(7, 5))
console.log(division(7, 5))

// Arrow Functions with Loops
// Pre-arrow function
const bears = ['Morgan', 'Amy', 'Lulu']
bears.forEach(function (bear) {
  console.log(`${bear} is my bestfriend`)
})

// Arrow Function
bears.forEach((bear) => {
  console.log(`${bear} is my bestbuddy!`)
})

// Default Function Argument Value
// Provide a default argument value if non is provided when the function is invoked

const messageForYou = (name = 'User') => {
  return `Good Evening, ${name}. Hope you are okay =>`
}

console.log(messageForYou())
console.log(messageForYou(singer3))

// Class-Based Object Blueprints
/*
  Allows creation/instantiation of object using classes as our blueprints

  Syntax:
  class Classname {
    constructor(objectPropertyA, objectPropertyB){
      this.objectPropertyA = objectPropertyA
      this.objectPropertyB = objectPropertyB
    }
  }
*/
class Car {
  constructor(brand, name, year) {
    this.brand = brand
    this.name = name
    this.year = year
  }
}

// Instantiating an object
/*
  The 'new' operator creates/instantiates a new object with the given arguments as the value of its properties
*/

// let/const variableName = new Classname()

const myCar = new Car()
console.log(myCar)

myCar.brand = 'Ford'
myCar.name = 'Everest'
myCar.year = '1996'

console.log(myCar)

const myNewCar = new Car('Toyota', 'Vios', 2000)
console.log(myNewCar)

/*
  Mini Activity #4
  create a `Character` Class Constructor

  name:
  role:
  strength:
  weakness:

  create two new characters out of a class constructor and save it in their respective variables
  - log the variables, send a screenshot
  
*/

class Character {
  constructor(name, role, strength, weakness) {
    this.name = name
    this.role = role
    this.strength = strength
    this.weakness = weakness
    this.introduce = () => {
      console.log(`Hi! I am ${this.name}`)
    }
  }
}

const police = new Character(
  'Cardo Dalisay',
  'Police',
  'Fearless',
  'Powerful Corrupt Politician(s)'
)
console.log(police)

const spiritFighter = new Character()

spiritFighter.name = 'Juan Dela Cruz'
spiritFighter.role = 'Spirit Fighter'
spiritFighter.strength = 'Fearless'
spiritFighter.weakness = 'Demi-Human'

console.log(spiritFighter)

const starlightAnya = new Character(
  'Starlight Anya',
  'Mage of Cuteness',
  'Mama and Papa',
  'Peanuts'
)

console.log(starlightAnya)
starlightAnya.introduce()
