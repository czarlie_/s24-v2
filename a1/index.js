// Exponent Operator
//getCube
let getCube = (number) => {
  // Template Literals
  console.log(`The cube of ${number} is ${number ** 3}`)
}
//log
getCube(2)

// Array Destructuring
//create an address array
const address = [258, 'Washington Avenue', 'North West', 'California', 90011]
//deconstruct the array
const [number, building, street, state, zip] = address
//log
console.log(`I live at ${number} ${building}, ${street}, ${state}, ${zip}`)

// Object Destructuring
//create an animal object
const animal = {}
//properties:
animal.name = 'lolong'
//name
//species
animal.species = 'saltwater crocodile'
//weightf
animal.weight = '1,075 kilogram'
//measurement
animal.measurement = '20 feet 3 inches'
//deconstruct the objet
const { name, species, weight, measurement } = animal

//log
console.log(
  `${name} is a ${species}. He weighed ${weight} with a measurement of ${measurement}`
)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5]

//forEach
// console.log('Traditional way of using .forEach()')
// numbers.forEach(function (number) {
//   console.log(number)
// })

console.log('ES6 way of using .forEach()')
numbers.forEach((number) => {
  console.log(number)
})

//reduce
// Traditional Way:
// let resultOfReducedNumbersArray = numbers.reduce(function (
//   accumulator,
//   currentValue
// ) {
//   return accumulator + currentValue
// })
// console.log(resultOfReducedNumbersArray)

// ES6 Way:
let reduceNumber = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue
)
console.log(reduceNumber)

// Javascript Classes (ES6)
//constructor
class Dog {
  constructor(name, age, breed) {
    this.name = name
    this.age = age
    this.breed = breed
  }
}

//instance
let myPet = new Dog('Frankie', 5, 'Miniature Dachshund')
console.log(myPet)
